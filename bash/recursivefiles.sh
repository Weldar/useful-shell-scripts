#!/bin/bash

count=$(ls -dq backup* | wc -l)
newfilename="backup${count}.png"
if [ $count -gt 0 ]
then
  mv poster.png ${newfilename}
  echo File renamed to $newfilename
fi
